from mpi4py import MPI
import time

comm = MPI.COMM_WORLD
rank = MPI.COMM_WORLD.Get_rank()


def sleep_for(t=10):
    time.sleep(t)


if rank == 0:
    sleep_for(5)
    req = comm.irecv(source=1)
    msg = req.wait()
    print(f"Elapsed time before receiving at host: {round(time.time() - msg, 2)}s.")

if rank == 1:
    req = comm.isend(time.time(), dest=0)
    req.wait()
    for t in range(10):
        print(f"Elapsed time at worker: {t+1}")
        sleep_for(1)