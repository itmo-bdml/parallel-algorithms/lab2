from mpi4py import MPI

rank = MPI.COMM_WORLD.Get_rank()
n = MPI.COMM_WORLD.Get_size()
comm = MPI.COMM_WORLD
"""
I tried to deal with the single message and back connection to inform the host
But it didn't work out. The host receives another message from the worker to
confirm receiving
"""

if rank == 0:
    msg = "Hello, python!"
    req = comm.isend(msg, dest=1)
    req.wait()
    req = comm.irecv(source=n-1)
    original_msg = req.wait()
    print(f"original message: {original_msg}\nDONE!")
else:
    req = comm.irecv(source=rank-1)
    msg = req.wait()
    print(f"worker rank: {rank}, msg: {msg}")
    if rank + 1 < n:
        req = comm.isend(msg, dest=rank+1)
    else:
        req = comm.isend(msg, dest=0)
    req.wait()
