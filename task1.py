from mpi4py import MPI

comm = MPI.COMM_WORLD
# Get my rank
rank = MPI.COMM_WORLD.Get_rank()


if rank == 0:
    message = "Hello, world!"
    req = comm.isend(message, dest=1, tag=42)
    req.wait()

if rank == 1:
    req = comm.irecv(source=0, tag=42)
    status = req.wait()
    print(status)
