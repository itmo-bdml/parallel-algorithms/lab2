import time
from mpi4py import MPI

rank = MPI.COMM_WORLD.Get_rank()
n = MPI.COMM_WORLD.Get_size()
comm = MPI.COMM_WORLD


if rank == 0:
    for i in range(1, n):
        req = comm.irecv(source=i)
        t = req.wait()
        print(f"worker: {i}, elapsed time: {(time.time() - t) * 1000}ms")
else:
    req = comm.isend(time.time(), dest=0)
