from mpi4py import MPI
import numpy as np


comm = MPI.COMM_WORLD
rank = MPI.COMM_WORLD.Get_rank()
n = MPI.COMM_WORLD.Get_size()


if rank == 0:
    v1 = np.ones(100_000)
    v2 = np.ones(100_000) * 2
    splitted1 = np.array_split(v1, n)
    splitted2 = np.array_split(v2, n)

    data = [np.stack((s1, s2), axis=0) for s1, s2 in zip(splitted1, splitted2)]
else:
    data = None

data = comm.scatter(data, root=0)
print(f"process: {rank}, received data of shape: {data.shape}")

res = np.sum(np.dot(data[0], data[1]))
gathered_res = MPI.COMM_WORLD.gather(res, root=0)

if rank == 0:
    for i, proc_res in enumerate(gathered_res):
        print(f"process {i}: results = {proc_res}")

    print(f"Global result: {np.sum(gathered_res)}")
