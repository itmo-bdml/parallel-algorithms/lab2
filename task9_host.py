from mpi4py import MPI
import sys

size = MPI.COMM_WORLD.Get_size()
rank = MPI.COMM_WORLD.Get_rank()

print(f"Host rank: {rank}")
print(size)
comm = MPI.COMM_WORLD.Spawn(sys.executable, args=["task9_worker.py"], maxprocs=size)

for i in range(size):
    req = comm.recv(source=MPI.ANY_SOURCE)
    print(f"Received message from worker {req}")

# comm.Disconnect()