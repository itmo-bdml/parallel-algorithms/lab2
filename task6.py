import sys
from mpi4py import MPI
import time
import numpy as np

cur_size = 1
MAX_SIZE = 50_000


comm = MPI.COMM_WORLD
rank = MPI.COMM_WORLD.Get_rank()


if rank == 0:
    while cur_size <= MAX_SIZE + 1:
        data = np.ones((cur_size))
        start_time = time.time()
        for i in range(10):
            comm.send(data, dest=1)
            req = comm.recv(source=1)

        finish_time = time.time()
        r = (2 * sys.getsizeof(data) * 10) / (finish_time - start_time) / 1024 / 1024
        print(f"{cur_size}, {sys.getsizeof(data)} (bytes). Bandwidth: {round(r, 2)}(MB/s)")
        # cur_size += 1000
        cur_size += 1000

if rank == 1:
    while cur_size <= MAX_SIZE + 1:
        for i in range(10):
            req = comm.recv(source=0)
            comm.send(req, dest=0)
        cur_size += 1000

