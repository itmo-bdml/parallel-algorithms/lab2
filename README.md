# Lab 3. MPI4PY

## Parallel Algorithms Analysis

Itmo university. 1st semester

Performed by: Gleb Mikloshevich J4132C

Lecturer: Timur Sokhin

## Part one
**Tasks:**
1. Basic example of message passing
2. Passing objects to multiple workers
3. Measuring message transfer time 
4. Asynchronous message passing
5. Distributed dot production with MPI: scatter-gather
6. Measuring the bandwidth
7. Circular message exchange
8. Asynchronous message passing with confirmation
9. different files: host and worker
