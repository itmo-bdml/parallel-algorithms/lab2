from mpi4py import MPI
import time

comm = MPI.COMM_WORLD
rank = MPI.COMM_WORLD.Get_rank()


def sleep_for(t=10):
    time.sleep(t)


if rank == 0:
    comm.isend("What's up?", dest=1, tag=42)
    req = comm.irecv(source=1)

    t = 0
    while not req.Test():
        print(f"Elapsed time at host: {5 * t + 1}")
        sleep_for(5)
        t += 1
    req.wait()
    print("Done!")


if rank == 1:
    sleep_for(25)
    req = comm.irecv(source=0, tag=42)
    msg = req.wait()
    print(f"received message at worker: {msg}")
    req = comm.isend(True, dest=0)
    req.wait()
