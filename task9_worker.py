from mpi4py import MPI

comm = MPI.Comm.Get_parent()
rank = comm.Get_rank()
print(f"Create worker: {rank}")
req = comm.send(rank, dest=0)
