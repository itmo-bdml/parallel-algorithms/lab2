from mpi4py import MPI
import numpy as np

rank = MPI.COMM_WORLD.Get_rank()
comm = MPI.COMM_WORLD


class CustomObject:
    def __init__(self, arg1, arg2):
        self.arg1 = arg1
        self.arg2 = arg2

    def __str__(self):
        return f"CustomObject. arg1: {self.arg1}, arg2: {self.arg2}"


object1 = [1, 2, 3]
object2 = CustomObject("Hi!", 500)
object3 = np.ones((1, 4))

list_of_objects = [object1, object2, object3]
if rank == 0:
    for i, obj in enumerate(list_of_objects):
        req = comm.isend(obj, dest=i+1, tag=0)

else:
    req = comm.irecv(source=0)
    status = req.wait()
    print(f"worker: {rank}, message: {status}")
